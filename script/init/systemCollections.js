const eventMapping = {
    properties: {
        accountId: { 'type': 'keyword' },
        selector: { 'type': 'keyword' },
        isActive: { type: 'boolean' },
        data: { type: 'text'},
        position: { 'type': 'geo_point' }
    }
};

const accountMapping = {
    properties: {
        accountId: { 'type': 'keyword' },
        name: { 'type': 'keyword' },
        isActive: { type: 'boolean' },
        gpxTrigger: {
            properties: {
             delay: { type: 'integer' }
            }		
        },
    }
};

async function insertAccounts(kuzzle, accounts) {
    const promises = accounts.map(async (account) => {
        await kuzzle.document.create(
            'system',
            'accounts', {
                accountId: account.accountId,
                name: account.name,
                isActive: account.isActive,
                gpxTrigger: account.gpxTrigger
            },
            account.accountId,
            {
                queuable: true,
                refresh: 'wait_for'
            }
        );
    })
    return await Promise.all(promises)
}

module.exports.prepareSystemCollections = async(kuzzle) => {
    const accounts = [
        {
            name: 'Italy',
            accountId: 'italy',
            isActive: true,
            gpxTrigger: {
                 delay: 1000
            },
        },
        {
            name: 'Croatia',
            accountId: 'croatia',
            isActive: true,
            gpxTrigger: {
                delay: 1000
           },
        },
        {
            name: 'Spain',
            accountId: 'spain',
            isActive: true,
            gpxTrigger: {
                delay: 0
           },
        },
        {
            name: 'Slovenia',
            accountId: 'slovenia',
            isActive: true,
            gpxTrigger: {
                delay: 1000
           },
        }
    ];
   
    await kuzzle.query({
        controller: 'admin',
        action: 'resetDatabase',
        refresh: 'wait_for'
    });

    await kuzzle.index.create('system');
    await kuzzle.collection.create('system', 'events', eventMapping);
    await kuzzle.collection.create('system', 'accounts', accountMapping);
    await insertAccounts(kuzzle, accounts)
}