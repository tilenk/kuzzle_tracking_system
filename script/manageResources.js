require('dotenv').config();
const { prepareSystemCollections } = require('./init/systemCollections.js');

// Loads the Kuzzle SDK modules
const {
  Kuzzle,
  WebSocket
} = require('kuzzle-sdk');

const kuzzle = new Kuzzle(
  new WebSocket(process.env.KUZZLE_BACKEND_DOMAIN_NAME, { port: process.env.KUZZLE_BACKEND_PORT })
);

// Adds a listener to detect connection problems
kuzzle.on('networkError', error => {
  console.error('Network Error:', error);
});

const isAdminCreated = async () => {
  try {
    const adminExists = await kuzzle.server.adminExists()
    if (!adminExists) {
      await kuzzle.security.createFirstAdmin('kuzzle', {
        content: {
          fullName: 'Kuzzle admin'
        },
        credentials: {
          local: {
            username: process.env.KUZZLE_BACKEND_ADMIN_USERNAME,
            password: process.env.KUZZLE_BACKEND_ADMIN_PASSWORD,
          }
        }
      },
        {
          queuable: true,
          reset: true
        });
    }
  } catch (err) {
    console.log("Error occured while creating admin", err)
  }
}

const run = async () => {
  try {
    // Connects to the Kuzzle server
    await kuzzle.connect();
    await isAdminCreated();
    await kuzzle.auth.login('local', {
      username: process.env.KUZZLE_BACKEND_ADMIN_USERNAME,
      password: process.env.KUZZLE_BACKEND_ADMIN_PASSWORD
    });
    await prepareSystemCollections(kuzzle);
    console.log('Tracking system ready!');
  } catch (error) {
    console.error("Error occured while populating data", error.message);
  } finally {
    kuzzle.disconnect();
  }
};

run();
