require('dotenv').config();
const parseGpx = require('parse-gpx');

// Loads the Kuzzle SDK modules
const {
    Kuzzle,
    WebSocket
} = require('kuzzle-sdk');

const kuzzle = new Kuzzle(
    new WebSocket(process.env.KUZZLE_BACKEND_DOMAIN_NAME, { port: process.env.KUZZLE_BACKEND_PORT })
);

const timer = ms => new Promise(res => setTimeout(res, ms))

const run = async () => {
    var myArgs = process.argv.slice(2);
    try {
        // Connects to the Kuzzle server
        await kuzzle.connect();
        await kuzzle.auth.login('local', {
            username: process.env.KUZZLE_BACKEND_ADMIN_USERNAME,
            password: process.env.KUZZLE_BACKEND_ADMIN_PASSWORD
        });

        let account = await kuzzle.document.get(
            'system',
            'accounts',
            myArgs[0]
        )

        const parsedGpx = await parseGpx(`./events/${myArgs[0]}.gpx`);
        for (const track of parsedGpx) {

            await timer(account._source.gpxTrigger.delay);
            kuzzle.document.create(
                'system',
                'events',
                {
                    accountId: account._source.accountId,
                    data: 'Gpx data',
                    position: {
                        "lat": track.latitude,
                        "lon": track.longitude
                    }
                }
            )
        }
        console.log('All events triggered!');
    } catch (error) {
        console.error("Error occured while trigger events", error.message);
    } finally {
        kuzzle.disconnect();
    }
};

run();
